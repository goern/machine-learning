from github_webhook import Webhook
from flask import Flask, jsonify, request
import requests
import json

app = Flask(__name__)
webhook = Webhook(app) # Defines '/postreceive' endpoint

@app.route("/")
def hello_world():
    return "just nothing!"

@app.route("/_healthz")
def healthz():
    return jsonify({'status': 'good'})

@app.route("/gitlab", methods=['POST'])
def gitlab_receiver():
    if request.method == "POST":
        payload = json.loads(request.data)
        
        if payload['object_kind'] in ['push', 'issue']:
            repo_meta = {
                'homepage': payload['repository']['homepage'],
            }
            
            return "OK"
            
        return json.dumps({'error': "wrong event type"})

@webhook.hook()
def on_push(data):
    print("Got push with: {0}".format(data))

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
# hooker

ˈhʊkə/Submit

noun

## 1. RUGBY
the player in the middle of the front row of the scrum, who tries to hook the ball.

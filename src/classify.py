#!/usr/bin/env python

"""
lets try to figure out if a build was successful, if so, dump some traceability information
"""

import re, json, os, sys

result = {}
last = ""
git_revision_id = ""

fname = sys.argv[1] 

result['finished'] = 'failure'
result['build_id'] = os.path.basename(fname)

with open(fname, 'rb') as fh:
    first = next(fh).decode()

    fh.seek(-1024, 2)
    last = fh.readlines()[-1].decode()

    if last.strip().endswith("SUCCESS"):
        result['finished'] = 'success'

jenkins_logfile = open(fname, "r")
revision_regex = re.compile(r'\b[0-9a-f]{5,40}\b')
image_sha_regex = re.compile(r'[a-f0-9]{64}')

for line in jenkins_logfile:
    revision = []

    if line.startswith('Checking out Revision'):
        revision = revision_regex.findall(line)

    image_sha = image_sha_regex.findall(line)
    if image_sha != [] and result['finished'] == 'success':
            result['container_image_sha256'] = image_sha[0]

    if revision != []:
        result['git_hash'] = revision[0]

print json.dumps(result)